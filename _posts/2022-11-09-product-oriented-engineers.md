---
layout: post
title: "How to grow a team of product-oriented engineers, and why it matters"
date: 2022-11-09
categories: management engineering leadership
published: true
---

When I became CTO at [uptime.ac](http://uptime.ac/), I started out with a (very) small team of two people. Over time, our product vision solidified, we got funding and traction, and so an important part of my job became scaling this team up to 20+ engineers, working hand-in-hand with a product team that went from 0 to 8. We did a lot of tinkering with the organization principles in these 5 years. In the end, one of the most impactful changes we made was to implement the principles behind Marty Cagan’s [empowered product teams](https://www.svpg.com/empowered-product-teams/). In a nutshell, Cagan recommends autonomous, pluridisciplinary product teams working towards a common product objective. This means that the engineers have to be close to the product, have a good understanding of the product vision, and be able to take initiative and make decisions about features and implementation. In this article, I’d like to share what this translated to in terms of hiring and career growth.

## Hiring product-oriented engineers

If you want to succeed in the empowered approach, you need the right kind of engineers. While having engineers that are extremely tech-focused and able to find the most elegant technical solutions is very useful, having engineers that know the product’s vision and are able to contribute to it is often way more powerful, especially when a lot if it still remains to be made real.

The first step is to attract and hire this kind of engineers. What I generally looked for at uptime was engineers that were able to understand their role within the organization and the impact of their team. What you want to look for:

- people that will list the product and vision among their key criteria for their next job, as opposed to “a job writing python with flask on a k8s infra”. This one is easy to spot but crucial
- people that are able to articulate their previous teams’ mission. Too often (more so for junior engineers, but not only), I’ve seen candidate who are barely able to explain what their team was responsible for and how it impacted their company’s mission. How can you be expected to contribute to a vision you are not keen enough to make your own and explain during an interview?
- people that are able to explain the impact of features they built. Not a lot of candidates have generally been in empowered product organizations so you won’t have many perfect examples, but asking questions such as “what was the main achievement of your team during that period?” will lead to very interesting results. For example, you’ll be able to differentiate between engineers that always put technical challenges forward (we were able to migrate from ec2 vms to kubernetes in under 3 months) and engineers that at least sometimes speak of achievements in terms of product impact (”we were able to increase customer sign-up by X% by developing feature Y”). These candidates will also be those who ask questions about the user and the impact of the product
- people that are able to justify their choices in terms of their role and their team’s mission. In the case of engineers, I like to ask questions such as “give me an example of a major technical change that you suggested” — which (if there’s any) allow you to understand how they explain such a change. Is it “I suggested migrated from lib A to lib B because lib B is newer and has more features” or “I suggested migrated from lib A to lib B because it will give us access to feature X, which should speed up page load by Z%, in line with our product objective”. Look at reasonings that give you the impression that the candidate knew why the change was necessary as opposed to other work that might impact their objective, as well as candidate that are able to articulate this correctly. This is a particularly useful skill which helps the team determine which area are worth investing time in (especially if the PM is non-technical)

In general, you’re looking for engineers that know their users and are able to demonstrate empathy towards them. You can even see that in how they approach bugs: did they feel like treating bugs was a necessary but painful chore, or a way to get close to users’ needs while improving the product’s quality?

## Growing product-oriented engineers

Whether your organisation is already setup to give product responsibility to engineers or not, you need your team members to become better product-oriented engineers. To do so, they need clarity, incentives and opportunities to grow.

Clarity & incentives are most easily solved through the design of their career path. Being very clear about what your company values in software engineers is crucial. In other words, if you want product-oriented engineers, design their job objectives to highlight product skills, and ensure they get regular constructive feedback about their involvement in their team’s product strategy.

Finally, you need to give engineers opportunities to be involved in the product side of their work. Reading product literature can give you a lot of ideas, but here are my favorites:

- build industry-specific knowledge and empathy by including customer interviews or shadowing in your onboarding process. At uptime, new engineers were highly encouraged to spend at least half a day with an elevator technician on the field to understand their work
- deepen user knowledge by including engineers in user interviews. At uptime, product designers would very often offer to include one of their engineer teammates in the user interviews they conducted
- improve their product sense by giving them ownership of features. Having engineers design how the feature will work based on the problem described by the product manager helps them grow into the product mindset. And it also enables PMs to spend time on other topics!

## Conclusion

A few closing words on this topic. First, this shift does not come for free: having product-oriented engineers will invariably mean that your engineers spend less time on technical stuff like managing debt. You will have to compensate that somehow: you can sacralise some time for this kind of work, have a few engineers or teams that are less involved in the product side, or organise technical sprints for example. If you have a bigger team, or a large amount of technical work, you can also invest in platform teams.

Second, while I initially though mostly about early stage startups (which are more my area of expertise) writing this article, it can probably apply to bigger companies as well (and I recommend the [post](https://skamille.medium.com/the-product-culture-shift-441c31a3fdf1) by the excellent Camille Fournier on the matter).

To sum it up: if you feel like the impact of what you’re delivering is slipping as your team grows, or if your product managers spend so much time defining precise specs they can’t manage experiments or be serious about user research, I think you should try giving more product ownership to your engineers. I’m sure you won’t regret it! And in any case, I’m curious to hear your thoughts on this.
