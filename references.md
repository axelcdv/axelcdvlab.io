---
layout: page
title: Useful references
permalink: /references/
---

A non-exhaustive list of blogs and other resources I read. This will be categorized at
some point when it's more complete.

- [High scalability](https://highscalability.com)
- [Stacey on IoT](https://staceyoniot.com/)
- [dev.to](https://dev.to)
- [the morning paper](https://blog.acolyer.org)
