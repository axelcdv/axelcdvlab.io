---
layout: page
title: About
permalink: /about/
---

CTO @ [Uptime.ac](https://uptime.ac), making sure you don't get stuck in an elevator. Previously: [Sen.se](https://sen.se). Education: Polytechnique / UCLA. IoT, Python.
