---
layout: post
title: "Building a blog with middleman, Gitlab CI and AWS"
date: 2017-05-28
---

So that's it, you want to build a blog. I'm not sure why you ended up here, but
the point of this post is to serve as a short reference for setting one up using [middleman](https://middlemanapp.com/),
[Gitlab-CI](https://about.gitlab.com/features/gitlab-ci-cd/) and
[AWS](https://aws.amazon.com/) S3 and Cloudfront. This comes from a recent
deployment using this solution (not this one, which only uses Jekyll, Gitlab CI
& pages at the time of writing).

## Why specifically these three?

I'm not going to sift through the zillion ways you can host and deploy it, as it's not the point of this post, but
here are a few reasons why we chose this stack specifically:

- **Middleman** is a pretty powerful static site generator. It has some advantages
    over Jekyll which was initially considered, namely the possibility of
    handling i18n pages natively. Pretty much all of this how-to applies to
    other generators, though.
- **Gitlab-ci** is a free, (relatively) easy-to-use, no-hassle CI system. Thanks to
    their free shared runner allocation, you can use it out-of-the box even with
    private repos, so long as you don't deploy too often (but by then you'd
    better have your own runners I guess). This tutorial uses the hosted version
    of Gitlab.
- **S3 & Cloudfront** give you more performance than just using a static page
    hosting site such as github pages or gitlab pages. Using a CDN for a personal blog is
    most definitely overkill (unless you're already famous), but for a company blog
    it might be worth the overhead.

## Start the project

For starters you'd like to have an actual blog to deploy. We'd have to name this
project for the rest of this tutorial, so for simplicity's sake we'll just name
it `blog`. Since we're using middleman, simply start as the official tutorial asks you to:

- Install middleman with `gem install middleman`
- Bootstrap the project with `middleman init blog`. This will setup the
    initial project layout, as well as a Gemfile to install your ruby
    dependencies
- Don't forget to start a git repo and commit the initial setup. The middleman
    helper already provides you a `.gitignore`

## Setup your project on Gitlab

- Now that you have the project setup, create a git repo on Gitlab. If it's a
    personal project, you'll have it setup on
    `https://gitlab.com/<username>/blog/`.
- Setup your git remote with `git remote add origin
    git@gitlab.com:<username>/blog`, then push the initial commit with `git push -u origin master`
- Now is the time you want to start using gitlab-ci. Since we do not have
    anything in place for deployment we'll just have the system build the static
    site. Write a `.gitlab-ci.yml` file in the root of your project with the
    following content:

```
image: ruby

build:
  stage: build
  script:
    - bundle exec middleman build --build-dir=public/
  artifacts:
    paths:
      - public
```
TODO: Explain, fix docker image

## Your AWS setup

TODO

### References

TODO
